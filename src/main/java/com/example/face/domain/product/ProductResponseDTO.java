package com.example.face.domain.product;

public record ProductResponseDTO (String id, String name, Integer price, String description){
}
