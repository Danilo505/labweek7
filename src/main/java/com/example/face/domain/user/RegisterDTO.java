package com.example.face.domain.user;

public record RegisterDTO(String login, String password, UserRole role) {
}
