package com.example.face.domain.user;

public record AuthenticationDTO(String login, String password) {
}
