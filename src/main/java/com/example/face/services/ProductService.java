package com.example.face.services;

import com.example.face.domain.product.Product;
import com.example.face.domain.product.ProductRequestDTO;
import com.example.face.domain.product.ProductResponseDTO;
import com.example.face.repositories.ProductRepository;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository repository){
        this.productRepository = repository;
    }

    public List<ProductResponseDTO> getAllProduct(){
        return convert(productRepository.findAll());
    }

    public List<ProductResponseDTO> convert(List<Product> products){
        return products
                .stream()
                .map(product -> new ProductResponseDTO(product.getId(), product.getName(), product.getPrice(), product.getDescription()))
                .collect(Collectors.toList());
    }

    public ProductResponseDTO productPost(ProductRequestDTO productRequestDTO){
        Product product = new Product(productRequestDTO);
        if (productRequestDTO.name() == null || productRequestDTO.price() <0)
            return null;
        productRepository.save(product);
        return new ProductResponseDTO(product.getId(), product.getName(), product.getPrice(), product.getDescription());
    }

    public ProductResponseDTO updateProduct(String id, ProductRequestDTO productRequestDTO){
        Product existingProduct = productRepository.findById(id).orElse(null);
        if (existingProduct == null){
            return null;
        }
        existingProduct.setName(productRequestDTO.name());
        existingProduct.setPrice(productRequestDTO.price());
        existingProduct.setDescription(productRequestDTO.description());
        Product updateProduct = productRepository.save(existingProduct);
        return new ProductResponseDTO(updateProduct.getId(), updateProduct.getName(), updateProduct.getPrice(), updateProduct.getDescription());
    }

    public boolean deleteProduct(String id){
        Product existingProduct = productRepository.findById(id).orElseThrow(null);
        if (existingProduct != null){
            productRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
