package com.example.face.domain.user;

public record LoginResponseDTO (String token) {
}
